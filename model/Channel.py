# class is defined using class keyword
class Channel:
    # data members of class
    name = ""
    icon = ""
    users = []
    messages = []
    tags = []

    # class default constructor
    def __init__(self, name):
        self.name = name

    # user defined function of class
    def func(self):
        print("After calling func() method..")
        print("My channel's name is", self.name)

    def join_user(self, user):
        self.users.append(user)

    def quit_user(self, user):
        self.users.remove(user)
