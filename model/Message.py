import time


# class is defined using class keyword
class Message:
    # data members of class
    message = ""  # attribute 1
    date = ""  # attribute 2
    user = ""
    channel = ""
    tags = []

    # class default constructor
    def __init__(self, message, user, channel):
        self.message = message
        self.date = time.gmtime()
        self.user = user
        self.channel = channel

    # user defined function of class
    def add_tag(self, tag):
        self.tags.append(tag)

    def get_message_time(self):
        return time.strftime("%H:%M:%S", self.date)
