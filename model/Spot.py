class Spot:
    users = {}
    channels = {}
    name = ""

    def __init__(self, name):
        self.name = name

    def join_user(self, user):
        self.users[user.name] = user

    def create_channel(self, channel):
        self.channels[channel.name] = channel

    def remove_channel(self, channel):
        self.channels.remove(channel)
