import json


# class is defined using class keyword
class User:
    # data members of class
    genre = "undefined"  # attribute 1
    name = ""  # attribute 2

    # class default constructor
    def __init__(self, name):
        self.name = name

    # user defined function of class
    def func(self):
        print("After calling func() method..")
        print("My user's name is", self.name)
        print("His color is", self.genre)

    def toJson(self):
        return json.dumps(self, default=lambda o: o.__dict__)
