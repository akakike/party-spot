from flask import Flask
from flask import render_template, request, session
from flask_cors import cross_origin

from model.Channel import Channel
from model.Message import Message
from model.Spot import Spot
from model.User import User

app = Flask(__name__)
app.secret_key = "ÑLKSAJLKJQOWIUKZXMNALKJSHALSKJH"
# cors = CORS(app, resources={r"/foo": {"origins": "*"}})
# app.config['CORS_HEADERS'] = 'Content-Type'

spot = Spot("mi spot")
i = 1

@app.route('/index')
def index_page():
    return render_template('index.html', name='Kike')


@app.route('/')
def main_page():
    global i

    if not session.__contains__('user'):
        username = 'Party_' + str(i)
        i += 1
        user = User(username)
        channel = Channel("general")
        spot.create_channel(channel)
        spot.join_user(user)
        channel.join_user(user)
        session['user'] = user.__dict__
    return render_template('main.html', name='Kike')


@app.route('/channels')
@cross_origin()
def channels():
    content = dict.fromkeys(spot.channels, "0")
    return render_template('fragments/channels.html', channels=content)


@app.route('/channel/<name>')
@cross_origin()
def channel_content(name):
    user = session['user']
    content = spot.channels[name]
    return render_template('channel_content.html', content=content, user=user)


@app.route('/message/<channel_name>', methods=['PUT'])
@cross_origin()
def new_message(channel_name):
    user = session['user']
    vchannel = spot.channels[channel_name]
    vchannel.messages.append(Message(request.form['message'], user, vchannel))
    return render_template('channel_content.html', content=vchannel, user=user)


@app.route('/users')
@cross_origin()
def users():
    content = dict.fromkeys(spot.users, "0")
    return render_template('fragments/users.html', users=content)


if __name__ == '__main__':
    app.run(debug=True)
