var barCount = 60;
var initialDateStr = '01 Apr 2017 00:00 Z';
var ctx = document.getElementById("average").getContext("2d");
ctx.canvas.width = 250;
ctx.canvas.height = 60;

var myChart = new Chart(ctx, {
  type: 'line',
  data: {
    datasets: [{
      label: '',
      data: [],
      backgroundColor: [
        'rgba(255, 99, 132, 0.2)',
        'rgba(54, 162, 235, 0.2)',
        'rgba(255, 206, 86, 0.2)',
        'rgba(75, 192, 192, 0.2)',
        'rgba(153, 102, 255, 0.2)',
        'rgba(255, 159, 64, 0.2)'
      ],
      borderColor: [
        'rgba(255,99,132,1)',
        'rgba(54, 162, 235, 1)',
        'rgba(255, 206, 86, 1)',
        'rgba(75, 192, 192, 1)',
        'rgba(153, 102, 255, 1)',
        'rgba(255, 159, 64, 1)'
      ],
      borderWidth: 0
    }]
  },
  options: {
    scales: {
        xAxes: {
           gridLines: {
              display: false
           }
        },
        yAxes: {
           gridLines: {
              display: false
           }
        }
    }
  }
});

function getAverageData(dateStr, count) {
    $.getJSON("http://127.0.0.1:5000/analysis", function(data){
        myChart.data.datasets[0].data = [];
        myChart.data.labels = [];
        data.items.forEach((value) => {
            var date = new Date(value["date"]*1000);
            myChart.data.labels.push("");
            myChart.data.datasets[0].data.push({
                    t: date,
                    y: value["price"]
            });

        });
        myChart.data.datasets[0].label = 'Average ' + myChart.data.datasets[0].data[myChart.data.datasets[0].data.length  -1].y.toFixed(2)
        myChart.update();
    });

}

var update = function() {
	var dataset = myChart.config.data.datasets[0];

	// candlestick vs ohlc
	var type = document.getElementById('type').value;
	dataset.type = type;

	// linear vs log
	var scaleType = document.getElementById('scale-type').value;
	chart.config.options.scales.y.type = scaleType;

	// color
	var colorScheme = document.getElementById('color-scheme').value;
	if (colorScheme === 'neon') {
		dataset.color = {
			up: '#01ff01',
			down: '#fe0000',
			unchanged: '#999',
		};
	} else {
		delete dataset.color;
	}

	// border
	var border = document.getElementById('border').value;
	var defaultOpts = Chart.defaults.elements[type];
	if (border === 'true') {
		dataset.borderColor = defaultOpts.borderColor;
	} else {
		dataset.borderColor = {
			up: defaultOpts.color.up,
			down: defaultOpts.color.down,
			unchanged: defaultOpts.color.up
		};
	}

	chart.update();
};

$( document ).ready(function() {
    console.log( "ready!" );
    getAverageData(initialDateStr, barCount);
    setInterval(function(){
        //code goes here that will be run every 5 seconds.
        getAverageData(initialDateStr, barCount);
    }, 10000);
});


//document.getElementById('update').addEventListener('click', update);
//
//document.getElementById('randomizeData').addEventListener('click', function() {
//	chart.data.datasets.forEach(function(dataset) {
//		dataset.data = getRandomData(initialDateStr, barCount);
//	});
//	update();
//});
