
function getKrakenBalance() {
    $.getJSON("http://127.0.0.1:5000/balance", function(data){
        data.result["XXBT"]
        $('#balanceBTC').html(parseFloat(data.result["XXBT"]).toFixed(5));
        $('#balanceEUR').html(data.result["ZEUR"]);
    });

}

$( document ).ready(function() {
    console.log( "ready!" );
    getKrakenBalance();
    setInterval(function(){
        //code goes here that will be run every 5 seconds.
        getKrakenBalance();
    }, 120000);
});


//document.getElementById('update').addEventListener('click', update);
//
//document.getElementById('randomizeData').addEventListener('click', function() {
//	chart.data.datasets.forEach(function(dataset) {
//		dataset.data = getRandomData(initialDateStr, barCount);
//	});
//	update();
//});
