var barCount = 60;
var initialDateStr = '01 Apr 2017 00:00 Z';
var ctx = document.getElementById('candle').getContext('2d');
ctx.canvas.width = 250;
ctx.canvas.height = 60;
var chart = new Chart(ctx, {
	type: 'candlestick',
	data: {
		datasets: [{
			label: 'BTC-EUR',
			data:[{
                    t: new Date(),
                    o: 2,
                    h: 2,
                    l: 2,
                    c: 2
                }]
		}]
	}
});

var getRandomInt = function(max) {
	return Math.floor(Math.random() * Math.floor(max));
};

function randomNumber(min, max) {
	return Math.random() * (max - min) + min;
}

function randomBar(date, lastClose) {
	var open = randomNumber(lastClose * 0.95, lastClose * 1.05).toFixed(2);
	var close = randomNumber(open * 0.95, open * 1.05).toFixed(2);
	var high = randomNumber(Math.max(open, close), Math.max(open, close) * 1.1).toFixed(2);
	var low = randomNumber(Math.min(open, close) * 0.9, Math.min(open, close)).toFixed(2);
	return {
		t: date.valueOf(),
		o: open,
		h: high,
		l: low,
		c: close
	};

}

function getKrakenData(dateStr, count) {
    $.getJSON("http://127.0.0.1:5000/prices", function(data){
        //console.log(data.result)
        chart.data.datasets[0].data = [];
        data.result["XXBTZEUR"].forEach((value) => {
            var date = new Date(value[0]*1000);
            //console.log(date);
            chart.data.datasets[0].data.push({
                    t: date.valueOf(),
                    o: value[1],
                    h: value[2],
                    l: value[3],
                    c: value[4]
            });

        });
        $('#lastPrice').html(data.result["XXBTZEUR"][data.result["XXBTZEUR"].length-1][4]+'€');
        $('#total').html(
            parseFloat(
                parseFloat(data.result["XXBTZEUR"][data.result["XXBTZEUR"].length-1][4])
                    *parseFloat($('#balanceBTC').html()).toFixed(5)
                + parseFloat($('#balanceEUR').html())
            ).toFixed(2) + '€'
            );
        document.title = $('#total').html() + '/' + $('#lastPrice').html() + ' - BTC Bot'
        chart.data.datasets[0].label = 'BTC-EUR ' + $('#lastPrice').html()
        chart.update();
    });

}

var update = function() {
	var dataset = chart.config.data.datasets[0];

	// candlestick vs ohlc
	var type = document.getElementById('type').value;
	dataset.type = type;

	// linear vs log
	var scaleType = document.getElementById('scale-type').value;
	chart.config.options.scales.y.type = scaleType;

	// color
	var colorScheme = document.getElementById('color-scheme').value;
	if (colorScheme === 'neon') {
		dataset.color = {
			up: '#01ff01',
			down: '#fe0000',
			unchanged: '#999',
		};
	} else {
		delete dataset.color;
	}

	// border
	var border = document.getElementById('border').value;
	var defaultOpts = Chart.defaults.elements[type];
	if (border === 'true') {
		dataset.borderColor = defaultOpts.borderColor;
	} else {
		dataset.borderColor = {
			up: defaultOpts.color.up,
			down: defaultOpts.color.down,
			unchanged: defaultOpts.color.up
		};
	}

	chart.update();
};

$( document ).ready(function() {
    console.log( "ready!" );
    getKrakenData(initialDateStr, barCount);
    setInterval(function(){
        //code goes here that will be run every 5 seconds.
        getKrakenData(initialDateStr, barCount);
    }, 10000);
});


//document.getElementById('update').addEventListener('click', update);
//
//document.getElementById('randomizeData').addEventListener('click', function() {
//	chart.data.datasets.forEach(function(dataset) {
//		dataset.data = getRandomData(initialDateStr, barCount);
//	});
//	update();
//});
