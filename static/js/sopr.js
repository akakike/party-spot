var barCount = 60;
var initialDateStr = '01 Apr 2017 00:00 Z';
var ctx = document.getElementById("sopr").getContext("2d");
ctx.canvas.width = 250;
ctx.canvas.height = 60;

var soprChart = new Chart(ctx, {
  type: 'bar',
  data: {
    datasets: [{
      label: '',
      data: [],
      borderWidth: 1
    }]
  },

   options: {
       scales: {
            xAxes: {
               gridLines: {
                  display: false
               }
            },
            yAxes: {
               gridLines: {
                  display: false
               }
            }
       }
   }
});

function getSoprData(dateStr, count) {
    $.getJSON("http://127.0.0.1:5000/trades", function(data){
        soprChart.data.datasets[0].data = [];
        soprChart.data.labels = [];
        data["result"].forEach((value) => {
            var date = new Date(value["date"]*1000);
            soprChart.data.labels.push("")
            soprChart.data.datasets[0].data.push({
                    y: value["value"]
            });

        });
        soprChart.data.datasets[0].label = 'SOPR ' + soprChart.data.datasets[0].data[soprChart.data.datasets[0].data.length  -1].y.toFixed(2)
        soprChart.update();
    });

}

var update = function() {
	var dataset = soprChart.config.data.datasets[0];

	// candlestick vs ohlc
	var type = document.getElementById('type').value;
	dataset.type = type;

	// linear vs log
	var scaleType = document.getElementById('scale-type').value;
	chart.config.options.scales.y.type = scaleType;

	// color
	var colorScheme = document.getElementById('color-scheme').value;
	if (colorScheme === 'neon') {
		dataset.color = {
			up: '#01ff01',
			down: '#fe0000',
			unchanged: '#999',
		};
	} else {
		delete dataset.color;
	}

	// border
	var border = document.getElementById('border').value;
	var defaultOpts = Chart.defaults.elements[type];
	if (border === 'true') {
		dataset.borderColor = defaultOpts.borderColor;
	} else {
		dataset.borderColor = {
			up: defaultOpts.color.up,
			down: defaultOpts.color.down,
			unchanged: defaultOpts.color.up
		};
	}

	chart.update();
};

$( document ).ready(function() {
    console.log( "ready!" );
    getSoprData(initialDateStr, barCount);
    setInterval(function(){
        //code goes here that will be run every 5 seconds.
        getSoprData(initialDateStr, barCount);
    }, 10000);
});


//document.getElementById('update').addEventListener('click', update);
//
//document.getElementById('randomizeData').addEventListener('click', function() {
//	chart.data.datasets.forEach(function(dataset) {
//		dataset.data = getRandomData(initialDateStr, barCount);
//	});
//	update();
//});
